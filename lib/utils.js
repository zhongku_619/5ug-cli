const path = require('path')
const program = require('commander')
const fs = require('fs')
const machineIdSync  = require('node-machine-id')
  const baseUrl = 'http://diyapi.5ug.com/Api'

 // const baseUrl = 'http://localhost:6800/Api'

exports.isPc = !!process.cwd().match(/zkpc/)

exports.resolve = (...p) => path.join(process.cwd(), ...p)

exports.prefix = require('superagent-prefix')(baseUrl)

exports.getToken = () => {
  try {
    var token= require(path.join(__dirname, '../.site.json'))
    token.machineId=machineIdSync.machineIdSync()
    return token
  } catch (err) {
    if (program.debug) console.error(err)
    console.log('请先登录')
    require('./login')()
  }
}