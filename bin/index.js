#!/usr/bin/env node

const program = require('commander')

program.option('--debug', 'debug mode', false)

program
  .command('commit [desc]')
  .description('提交项目所有的文件(src/admins,src/components 两个文件下面的文件除外),服务器会自动记录文件的版本增量记录')
  .action(desc => {
    require('../lib/commit')(desc)
  })

program
  .command('admin')
  .description(`管理员操作功能[/Api/GitFile/Admin]:
  1. 更新客户端所有组件的property.json配置
  2. 组件Id、文件名同步
  3. 设置所有模块(widget的)CodeType
  4. 设置所有模板(SiteTheme的)CodeType,以及删除userId=0的模板
  5. 处理系统模块没有默认数据的[IWidgetSystemDataService.InitDefaultData]
  6. 为组件加入模板AutoForm，并更新组件表单Id
  7. 为模块加入AutoFormId`)
  .action(() => {
    require('../lib/admin')()
  })
program
  .command('build')
  .description('构建项目')
  .action(() => {
    require('../lib/create')('name', 'Build')
  })

program
  .command('create <name>')
  .description(`创建组件,<name>为组件名称:
  1.自动生成组件标准文件;
  2.自动构建main.js文件。示范：5ug create common/zk-test`)
  .action(name => {
    require('../lib/create')(name, 'CreateComponent')
  })

program
  .command('icon <themeId>')
  .description(`打包图标
  1.根据模板打包图标。示范：5ug icon 5d1d4723843bd01b346ec488
  2.5ug icon all 打包所有图标`)
  .action(themeId => {
    require('../lib/icon')(themeId /*  */ )
  })

program
  .command('init')
  .description('初始化项目')
  .action(() => {
    require('../lib/init')()
  })

program
  .command('login')
  .description('用户登录')
  .action(() => {
    require('../lib/login')()
  })

program
  .command('main')
  .description('生成 main.js')
  .action(() => {
    require('../lib/main')()
  })

program
  .command('main1')
  .description('生成 main.js')
  .action(() => {
    require('../lib/main1')()
  })

program
  .command('main3')
  .description('生成 main.js')
  .action(() => {
    require('../lib/main3')()
  })

program
  .command('publish <name>')
  .description(`管理员组件相关功能[/Api/Site5ug/Publish]:
  1. 发布单个组件，示范：5ug publish activitys/zk-groupbuy其中 publish all 发布所有组件
  2. 发布所有组件，5ug publish all
  3. 设置组件的CodeType、版本号、所有文件更新、风格记录、风格数量等
  4. 添加默认模块(widget)，如果组件模块不存在时，添加组件默认模块`)
  .action(name => {
    require('../lib/publish')(name)
  })

program.parse(process.argv)
