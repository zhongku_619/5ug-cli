
## 版本更新
- 0.5.5 优化5ug admin 以及帮助提示文字
- 0.4.4 优化5ug commit 命令
- 0.4.2 新增init和commit函数
- 0.4.1 优化组件发布，pc和移动端分别发布，发布需要验证用户
- 0.3.8 修改服务器地址
- 0.3.7 更新sign和token双重安全验证机制



## 5ug-cli 命令使用

- 全局安装 npm install 5ug-cli -g

### 登录：5ug login

-输入用户名，密码，选择项目

### 创建组件：5ug create <name>

-例如： 5ug create test/zk-test 创建组件 其中 test/zk-test 为组件路径和格式

### 发布组件：5ug publish <name>

- 功能：1.发布源文件 2.发布风格 3.版本统计
- 例如：5ug publish test/zk-test 创建组件 其中 test/zk-test 为组件路径和格式

### 发布所有组件：5ug publish all

### 构建 main.js 5ug main

- 遍历当前项目中 src/components src/elements src/admins 三个目录，自动生成 main.js
- 只支持二级目录，不支持一级目录和三级目录

## 创建模块流程

### 第一步 创建组件：5ug create <name>

- 本地生成模板文件，包括 vue,js,scss 等文件

### 第二步 发布组件：5ug publish <name>

- 在服务器中创建组件，包括风格、样式、版本等

